<table>
  <tr>
    <th>Project</th>
    <th>Environment</th>
    <th>Data1</th>
    <th>Multiple Data</th>
  </tr>
  <tr>
    <td rowspan="3">project1</td>
    <td rowspan="3">prod</td>
    <td rowspan="3">project1data</td>
    <td>project1 multi row data1</td>
  </tr>
  <tr>
    <td>project1 multi row data2</td>
  </tr>
  <tr>
    <td>project1 multi row data3</td>
  </tr>
  <tr>
    <td rowspan="3">project2</td>
    <td rowspan="3">stage</td>
    <td rowspan="3">project2data</td>
    <td>project2 multi row data1</td>
  </tr>
  <tr>
    <td>project2 multi row data2</td>
  </tr>
  <tr>
    <td>project2 multi row data3</td>
  </tr>
  <tr>
    <td rowspan="3">project3</td>
    <td rowspan="3">test</td>
    <td rowspan="3">project3data</td>
    <td>project3 multi row data1</td>
  </tr>
  <tr>
    <td>project3 multi row data2</td>
  </tr>
  <tr>
    <td>project3 multi row data3</td>
  </tr>
  <tr>
    <td rowspan="3">project4</td>
    <td rowspan="3">dev</td>
    <td rowspan="3">project4data</td>
    <td>project4 multi row data1</td>
  </tr>
  <tr>
    <td>project4 multi row data2</td>
  </tr>
  <tr>
    <td>project4 multi row data3</td>
  </tr>
  <tr>
    <td rowspan="3">project5</td>
    <td rowspan="3">qa</td>
    <td rowspan="3">project5data</td>
    <td>project5 multi row data1</td>
  </tr>
  <tr>
    <td>project5 multi row data2</td>
  </tr>
  <tr>
    <td>project5 multi row data3</td>
  </tr>

</table>



| A                       | B   |
| ----------------------- | --- |
| 1<td rowspan="2">3</td> | 2   |
| 4                       | 5   |
| 6                       | 7   |
